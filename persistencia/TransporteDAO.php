<?php

class TransporteDAO{

    private $idtransporte;
    private $serial;
    private $modelo;
    private $fotoTransporte;
    private $fotoCartaPropiedad;
    private $descripcion;
    private $estado;
    private $idcolor;
    private $idtipo;
    private $idUsuario;
    private $idmarca;
    private $idParqueadero;

    function TransporteDAO($idtransporte= "", $serial= "", $modelo= "", $fotoTransporte= "", $fotoCartaPropiedad= "", $descripcion= "", 
                            $estado= "", $idcolor= "", $idtipo= "", $idUsuario= "", $idmarca= "", $idParqueadero= ""){        
        $this -> idtransporte = $idtransporte;
        $this -> serial = $serial;
        $this -> modelo = $modelo;
        $this -> fotoTransporte = $fotoTransporte;
        $this -> fotoCartaPropiedad = $fotoCartaPropiedad;
        $this -> descripcion = $descripcion;
        $this -> estado = $estado;
        $this -> idcolor = $idcolor;
        $this -> idtipo = $idtipo;
        $this -> idUsuario = $idUsuario;
        $this -> idmarca = $idmarca;
        $this -> idParqueadero = $idParqueadero;
        
    }
    

    function registrar(){
        return   "insert into transporte (serial, modelo, descripcion, idColor, idTipo, idUsuario, idMarca, idParqueadero, fotoTransporte, fotoCartaPropiedad)
                values ('" . $this-> serial . "', '" . $this-> modelo . "', '".$this -> descripcion . "',
                        '" .$this -> idcolor . "', '" .$this -> idtipo ."','" .$this -> idUsuario ."',
                        '" .$this -> idmarca . "', '" .$this -> idParqueadero ."',
                        '". $this -> fotoTransporte . "',
                        '". $this -> fotoCartaPropiedad . "')";
    }

    function actualizar(){
        return "update transporte set 
                serial = '" . $this -> serial . "',
                modelo = '" . $this -> modelo . "', 
                fotoTransporte = '" . $this -> fotoTransporte . "',
                fotoCartaPropiedad = '" . $this -> fotoCartaPropiedad . "',
                descripcion = '" . $this -> descripcion . "',
                estado  ='" . $this -> estado . "',
                idColor = '" . $this -> idcolor . "',
                idTipo ='" . $this -> idtipo . "',

                where idtransporte=" . $this -> idtransporte;
    }
    
    function actualizarFotoTransporte(){
        return "update transporte set
                fotoTransporte = '" . $this -> fotoTransporte . "'
                where idtransporte=" . $this -> idtransporte;
    }

    function actualizarFotoCartaPropiedad(){
        return "update transporte set
                fotoCartaPropiedad = '" . $this -> fotoCartaPropiedad . "'
                where idtransporte=" . $this -> idtransporte;
    }

    function existeSerial(){
        return "select idtransporte 
                from transporte
                where serial = '" . $this->serial . "'";
    }

    function actualizarEstado(){
        return "update transporte set
                estado = '" . $this -> estado . "'
                where idtransporte=" . $this -> idtransporte;
    }
    
    function consultar() {
        echo "select serial, modelo, fotoTransporte, fotoCartaPropiedad, 
                        descripcion, estado, idcolor, idtipo, idmarca
                from transporte
                where 	idtransporte =" . $this -> idtransporte;
    }

    function consultarTodos(){
        return "select idtransporte	,serial, fotoTransporte, idTipo,idUsuario,idMarca,idParqueadero	
                from transporte
                order by idtransporte";
    }
    function consultarFotosVehiculo(){
       return "select  fotoTransporte, fotoCartaPropiedad
                from transporte
                where idUsuario=" . $this -> idUsuario . " and idMarca=" . $this -> idtipo;
    }
    function consultarTransportesPorId($id){
        return "select *
                from transporte
                where idUsuario =". $id ."
                order by idtransporte";
    }
}

?>
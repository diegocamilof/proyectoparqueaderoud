-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-03-2022 a las 02:27:04
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `parqueaderoud`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `idadmin` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`idadmin`, `nombre`, `apellido`, `correo`, `password`) VALUES
(1, 'Andrés ', 'Soto', '123@123.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `celador`
--

CREATE TABLE `celador` (
  `idcelador` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `numeroID` int(15) DEFAULT NULL,
  `foto` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `idTipoIdentificacion` int(11) NOT NULL,
  `idFacultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `celador`
--

INSERT INTO `celador` (`idcelador`, `nombre`, `apellido`, `correo`, `password`, `direccion`, `telefono`, `numeroID`, `foto`, `estado`, `idTipoIdentificacion`, `idFacultad`) VALUES
(1, 'CELADOR Sin', 'Asignar ', 'csa@csa.com', 'fdcde0ec8cfe1e177670f658ae15721e', 'sin asignar', '00', 0, NULL, 0, 1, 1),
(2, 'Peter', 'Parker', 'pp@pp.com', 'c483f6ce851c9ecd9fb835ff7551737c', NULL, NULL, NULL, NULL, 0, 1, 1),
(5, 'javier', 'romero', 'jr@jr.com', 'd5de679d452cb429e6f55e64a9988cbf', NULL, NULL, NULL, NULL, 0, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color`
--

CREATE TABLE `color` (
  `idcolor` int(11) NOT NULL,
  `color` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `color`
--

INSERT INTO `color` (`idcolor`, `color`) VALUES
(1, 'Sin Asignar'),
(2, 'Rojo'),
(3, 'Azul'),
(4, 'Amarillo'),
(5, 'Negro'),
(6, 'Café');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultad`
--

CREATE TABLE `facultad` (
  `idfacultad` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `facultad`
--

INSERT INTO `facultad` (`idfacultad`, `nombre`) VALUES
(1, 'Sin Asignar'),
(2, 'Facultad Ciencia y Educación'),
(3, 'Facultad Ingeniería'),
(4, 'Facultad Tecnológica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gerente`
--

CREATE TABLE `gerente` (
  `idgerente` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `numeroID` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `idtipoIdentificacion` int(11) NOT NULL,
  `idFacultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `gerente`
--

INSERT INTO `gerente` (`idgerente`, `nombre`, `apellido`, `correo`, `password`, `numeroID`, `idtipoIdentificacion`, `idFacultad`) VALUES
(1, 'Gerente. Mario ', 'Mendoza', 'mm@mm.com', 'b3cd915d758008bd19d0f2428fbb354a', '123456', 3, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `idmarca` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `idTipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idmarca`, `nombre`, `idTipo`) VALUES
(1, 'Sin Asignar', 1),
(2, 'Benotto', 2),
(3, 'GW Bicycle', 2),
(4, 'Pulsar', 3),
(5, 'Yamaha', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parqueadero`
--

CREATE TABLE `parqueadero` (
  `idparqueadero` int(11) NOT NULL,
  `numero` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `puestosOcupados` int(11) NOT NULL,
  `puestosMaximos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `parqueadero`
--

INSERT INTO `parqueadero` (`idparqueadero`, `numero`, `estado`, `puestosOcupados`, `puestosMaximos`) VALUES
(1, 'Sin Asignar', 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `idproyecto` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `idFacultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`idproyecto`, `nombre`, `idFacultad`) VALUES
(1, 'Sin Asignar', 1),
(2, 'Licenciatura en Lengua Castellana', 2),
(3, 'Licenciatura en Física', 2),
(4, 'Ingeniería Civil ', 3),
(5, 'Ingeniería de Mecánica', 3),
(6, 'Tecnología en Sistematización de Datos', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `idtipo` int(11) NOT NULL,
  `tipo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`idtipo`, `tipo`) VALUES
(1, 'Sin Asignar'),
(2, 'Bicicleta'),
(3, 'Moto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoidentificacion`
--

CREATE TABLE `tipoidentificacion` (
  `idtipoIdentificacion` int(11) NOT NULL,
  `nombreTipo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tipoidentificacion`
--

INSERT INTO `tipoidentificacion` (`idtipoIdentificacion`, `nombreTipo`) VALUES
(1, 'Sin Asignar'),
(2, 'Tarjeta de Identidad'),
(3, 'Cédula de Ciudadanía');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transporte`
--

CREATE TABLE `transporte` (
  `idtransporte` int(11) NOT NULL,
  `serial` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `modelo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `fotoTransporte` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fotoCartaPropiedad` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `idColor` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idMarca` int(11) NOT NULL,
  `idParqueadero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `transporte`
--

INSERT INTO `transporte` (`idtransporte`, `serial`, `modelo`, `fotoTransporte`, `fotoCartaPropiedad`, `descripcion`, `estado`, `idColor`, `idTipo`, `idUsuario`, `idMarca`, `idParqueadero`) VALUES
(1, 'sin asignar', 'sin asignar', NULL, 'sinasignar', 'sin asignar', 0, 1, 1, 1, 1, 1),
(3, '123456', '2020', NULL, '', 'Cicla montaña rin 22', 0, 2, 0, 2, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `codigoEstudiantil` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `foto` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `numeroID` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `idProyecto` int(11) NOT NULL,
  `idTipoIdentificacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `apellido`, `correo`, `password`, `codigoEstudiantil`, `direccion`, `telefono`, `foto`, `estado`, `numeroID`, `idProyecto`, `idTipoIdentificacion`) VALUES
(1, 'Sin', 'Asignar', 'sa@sa.com', 'c12e01f2a13ff5587e1e9e4aedb8242d', '00', '00', '00', NULL, 0, '00', 1, 1),
(2, 'Jair', 'Sánchez', 'jg@jg.com', '1272c19590c3d44ce33ba054edfb9c78', '20171778039', 'Carrera 15', '7500500', NULL, 1, '1233491408', 6, 3),
(3, 'Cristiano', 'Ronaldo', 'cr7@cr7.com', 'c9178aa682eadb31aa6d77e85c8cd9c6', NULL, NULL, NULL, NULL, 2, NULL, 1, 1),
(4, 'Diego', 'Fernandez', 'df@df.com', 'eff7d5dba32b4da32d9a67a519434d3f', NULL, NULL, NULL, NULL, 2, NULL, 1, 1),
(6, 'Sergio', 'Amaru', 'su@su.com', '0b180078d994cb2b5ed89d7ce8e7eea2', NULL, NULL, NULL, NULL, 0, NULL, 1, 1),
(7, 'Brayan', 'Sanchez', 'bs@bs.com', '7c9df801238abe28cae2675fd3166a1a', NULL, NULL, NULL, NULL, 0, NULL, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indices de la tabla `celador`
--
ALTER TABLE `celador`
  ADD PRIMARY KEY (`idcelador`),
  ADD KEY `fk_celador_Facultad1_idx` (`idFacultad`),
  ADD KEY `fk_celador_tipoIdentificacion1_idx` (`idTipoIdentificacion`);

--
-- Indices de la tabla `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`idcolor`);

--
-- Indices de la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD PRIMARY KEY (`idfacultad`);

--
-- Indices de la tabla `gerente`
--
ALTER TABLE `gerente`
  ADD PRIMARY KEY (`idgerente`,`idtipoIdentificacion`),
  ADD KEY `fk_gerente_Facultad1_idx` (`idFacultad`),
  ADD KEY `fk_gerente_tipoIdentificacion1_idx` (`idtipoIdentificacion`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idmarca`),
  ADD KEY `fk_marca_tipo1_idx` (`idTipo`);

--
-- Indices de la tabla `parqueadero`
--
ALTER TABLE `parqueadero`
  ADD PRIMARY KEY (`idparqueadero`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`idproyecto`),
  ADD KEY `fk_proyecto_Facultad_idx` (`idFacultad`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indices de la tabla `tipoidentificacion`
--
ALTER TABLE `tipoidentificacion`
  ADD PRIMARY KEY (`idtipoIdentificacion`);

--
-- Indices de la tabla `transporte`
--
ALTER TABLE `transporte`
  ADD PRIMARY KEY (`idtransporte`,`idParqueadero`),
  ADD KEY `fk_transporte_color1_idx` (`idColor`),
  ADD KEY `fk_transporte_tipo1_idx` (`idTipo`),
  ADD KEY `fk_transporte_usuario1_idx` (`idUsuario`),
  ADD KEY `fk_transporte_marca1_idx` (`idMarca`),
  ADD KEY `fk_transporte_parqueadero1_idx` (`idParqueadero`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `fk_usuario_proyecto1_idx` (`idProyecto`),
  ADD KEY `fk_usuario_tipoIdentificacion1_idx` (`idTipoIdentificacion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `idadmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `celador`
--
ALTER TABLE `celador`
  MODIFY `idcelador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `color`
--
ALTER TABLE `color`
  MODIFY `idcolor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `facultad`
--
ALTER TABLE `facultad`
  MODIFY `idfacultad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `gerente`
--
ALTER TABLE `gerente`
  MODIFY `idgerente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `idmarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `parqueadero`
--
ALTER TABLE `parqueadero`
  MODIFY `idparqueadero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `idproyecto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `idtipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipoidentificacion`
--
ALTER TABLE `tipoidentificacion`
  MODIFY `idtipoIdentificacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `transporte`
--
ALTER TABLE `transporte`
  MODIFY `idtransporte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `celador`
--
ALTER TABLE `celador`
  ADD CONSTRAINT `fk_celador_Facultad1` FOREIGN KEY (`idFacultad`) REFERENCES `facultad` (`idfacultad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_celador_tipoIdentificacion1` FOREIGN KEY (`idTipoIdentificacion`) REFERENCES `tipoidentificacion` (`idtipoIdentificacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gerente`
--
ALTER TABLE `gerente`
  ADD CONSTRAINT `fk_gerente_Facultad1` FOREIGN KEY (`idFacultad`) REFERENCES `facultad` (`idfacultad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gerente_tipoIdentificacion1` FOREIGN KEY (`idtipoIdentificacion`) REFERENCES `tipoidentificacion` (`idtipoIdentificacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `marca`
--
ALTER TABLE `marca`
  ADD CONSTRAINT `fk_marca_tipo1` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idtipo`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

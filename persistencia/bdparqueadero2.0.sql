-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-11-2021 a las 16:30:33
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdparqueadero`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `idadmin` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`idadmin`, `nombre`, `apellido`, `correo`, `password`) VALUES
(1, 'Diego', 'Fernandez', '123@123.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `celador`
--

CREATE TABLE `celador` (
  `idcelador` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` int(10) DEFAULT NULL,
  `numeroID` int(15) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `idTipoIdentificacion` int(11) NOT NULL,
  `idFacultad` int(11) NOT NULL,
  `idParqueadero` int(11) NOT NULL,
  `idTransporte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `celador`
--

INSERT INTO `celador` (`idcelador`, `nombre`, `apellido`, `correo`, `password`, `direccion`, `telefono`, `numeroID`, `foto`, `estado`, `idTipoIdentificacion`, `idFacultad`, `idParqueadero`, `idTransporte`) VALUES
(1, 'Sin', 'Asignar', 'sa@sa.com', 'c12e01f2a13ff5587e1e9e4aedb8242d', 'sin asignar', 0, 0, NULL, NULL, 1, 1, 1, 1),
(3, 'buena', 'vista', 'bv@bv.com', '121aa3ee4a7d5b1bbbc760fd0c6de79b', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1),
(4, 'Ozzy', 'Osbourne', 'oo@oo.com', 'e47ca7a09cf6781e29634502345930a7', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color`
--

CREATE TABLE `color` (
  `idcolor` int(11) NOT NULL,
  `color` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `color`
--

INSERT INTO `color` (`idcolor`, `color`) VALUES
(1, 'sin asignar'),
(2, 'Rojo'),
(3, 'Negro'),
(4, 'Amarilo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultad`
--

CREATE TABLE `facultad` (
  `idFacultad` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `facultad`
--

INSERT INTO `facultad` (`idFacultad`, `nombre`) VALUES
(1, 'Ninguno'),
(2, 'Facultad Ingeniería'),
(3, 'Facultad Ciencia y Educación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gerente`
--

CREATE TABLE `gerente` (
  `idgerente` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `idFacultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `idmarca` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idmarca`, `nombre`) VALUES
(1, 'sin asignar'),
(2, 'GW Bicycle'),
(3, 'Benotto'),
(4, 'AKT'),
(5, 'Pulsar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parqueadero`
--

CREATE TABLE `parqueadero` (
  `idparqueadero` int(11) NOT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `idTransporte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `parqueadero`
--

INSERT INTO `parqueadero` (`idparqueadero`, `numero`, `estado`, `idTransporte`) VALUES
(1, 'sin asignar', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `idproyecto` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `idFacultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`idproyecto`, `nombre`, `idFacultad`) VALUES
(1, 'Sin asignar', 1),
(2, 'Ingeniería de Sistemas', 2),
(3, 'Ingeniería Civil ', 2),
(4, 'Licenciatura en Física', 3),
(5, 'Licenciatura en Español', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `idtipo` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`idtipo`, `tipo`) VALUES
(1, 'sin asignar'),
(2, 'Moto'),
(3, 'Bicicleta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoidentificacion`
--

CREATE TABLE `tipoidentificacion` (
  `idtipoIdentificacion` int(11) NOT NULL,
  `nombreTipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipoidentificacion`
--

INSERT INTO `tipoidentificacion` (`idtipoIdentificacion`, `nombreTipo`) VALUES
(1, 'Sin asignar'),
(2, 'Tarjeta de Identidad'),
(3, 'Cédula de Ciudadanía');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transporte`
--

CREATE TABLE `transporte` (
  `idtransporte` int(11) NOT NULL,
  `serial` varchar(45) DEFAULT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `fotoPropiedad` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `idcolor` int(11) NOT NULL,
  `idmarca` int(11) NOT NULL,
  `idtipo` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `transporte`
--

INSERT INTO `transporte` (`idtransporte`, `serial`, `modelo`, `foto`, `fotoPropiedad`, `descripcion`, `estado`, `idcolor`, `idmarca`, `idtipo`, `idusuario`) VALUES
(1, 'sin asignar', 'sin asignar', NULL, NULL, 'sin asignar', NULL, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `codigoEstudiantil` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` int(10) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `numeroID` int(15) DEFAULT NULL,
  `idProyecto` int(11) NOT NULL,
  `idTipoIdentificacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `apellido`, `correo`, `password`, `codigoEstudiantil`, `direccion`, `telefono`, `foto`, `estado`, `numeroID`, `idProyecto`, `idTipoIdentificacion`) VALUES
(1, 'Sin', 'Asignar', 's@s.com', 's', 'sin asignar', 'sin asignar', 0, NULL, NULL, 0, 1, 1),
(3, 'jair', 'gutierrez', '20@20.com', '98f13708210194c475687be6106a3b84', NULL, NULL, NULL, NULL, 0, NULL, 1, 1),
(7, 'Martin', 'Arias', '10@10.com', 'd3d9446802a44259755d38e6d163e820', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1),
(8, 'Juan', 'Alberto', 'ja@ja.com', 'a78c5bf69b40d464b954ef76815c6fa0', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1),
(9, 'Otoniel', 'Ortega', 'ot@ot.com', '15773549ac72a773120e125f74b04393', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indices de la tabla `celador`
--
ALTER TABLE `celador`
  ADD PRIMARY KEY (`idcelador`,`idTipoIdentificacion`,`idFacultad`,`idParqueadero`,`idTransporte`),
  ADD KEY `fk_celador_tipoIdentificacion1_idx` (`idTipoIdentificacion`),
  ADD KEY `fk_celador_Facultad1_idx` (`idFacultad`),
  ADD KEY `fk_celador_parqueadero1_idx` (`idParqueadero`,`idTransporte`);

--
-- Indices de la tabla `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`idcolor`);

--
-- Indices de la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD PRIMARY KEY (`idFacultad`);

--
-- Indices de la tabla `gerente`
--
ALTER TABLE `gerente`
  ADD PRIMARY KEY (`idgerente`,`idFacultad`),
  ADD KEY `fk_gerente_Facultad1_idx` (`idFacultad`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idmarca`);

--
-- Indices de la tabla `parqueadero`
--
ALTER TABLE `parqueadero`
  ADD PRIMARY KEY (`idparqueadero`,`idTransporte`),
  ADD KEY `fk_parqueadero_transporte1_idx` (`idTransporte`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`idproyecto`),
  ADD KEY `fk_proyecto_Facultad_idx` (`idFacultad`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indices de la tabla `tipoidentificacion`
--
ALTER TABLE `tipoidentificacion`
  ADD PRIMARY KEY (`idtipoIdentificacion`);

--
-- Indices de la tabla `transporte`
--
ALTER TABLE `transporte`
  ADD PRIMARY KEY (`idtransporte`,`idusuario`),
  ADD KEY `fk_transporte_color1_idx` (`idcolor`),
  ADD KEY `fk_transporte_marca1_idx` (`idmarca`),
  ADD KEY `fk_transporte_tipo1_idx` (`idtipo`),
  ADD KEY `fk_transporte_usuario1_idx` (`idusuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`,`idProyecto`,`idTipoIdentificacion`),
  ADD KEY `fk_usuario_proyecto1_idx` (`idProyecto`),
  ADD KEY `fk_usuario_tipoIdentificacion1_idx` (`idTipoIdentificacion`);

--

--
-- Filtros para la tabla `celador`
--
ALTER TABLE `celador`
  ADD CONSTRAINT `fk_celador_Facultad1` FOREIGN KEY (`idFacultad`) REFERENCES `facultad` (`idFacultad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_celador_parqueadero1` FOREIGN KEY (`idParqueadero`,`idTransporte`) REFERENCES `parqueadero` (`idparqueadero`, `idTransporte`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_celador_tipoIdentificacion1` FOREIGN KEY (`idTipoIdentificacion`) REFERENCES `tipoidentificacion` (`idtipoIdentificacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gerente`
--
ALTER TABLE `gerente`
  ADD CONSTRAINT `fk_gerente_Facultad1` FOREIGN KEY (`idFacultad`) REFERENCES `facultad` (`idFacultad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `parqueadero`
--
ALTER TABLE `parqueadero`
  ADD CONSTRAINT `fk_parqueadero_transporte1` FOREIGN KEY (`idTransporte`) REFERENCES `transporte` (`idtransporte`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_proyecto_Facultad` FOREIGN KEY (`idFacultad`) REFERENCES `facultad` (`idFacultad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `transporte`
--
ALTER TABLE `transporte`
  ADD CONSTRAINT `fk_transporte_color1` FOREIGN KEY (`idcolor`) REFERENCES `color` (`idcolor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transporte_marca1` FOREIGN KEY (`idmarca`) REFERENCES `marca` (`idmarca`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transporte_tipo1` FOREIGN KEY (`idtipo`) REFERENCES `tipo` (`idtipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transporte_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_proyecto1` FOREIGN KEY (`idProyecto`) REFERENCES `proyecto` (`idproyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_tipoIdentificacion1` FOREIGN KEY (`idTipoIdentificacion`) REFERENCES `tipoidentificacion` (`idtipoIdentificacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
require 'persistencia/GerenteDAO.php';
require_once 'persistencia/Conexion.php';
class Gerente extends Persona{
    private $idFacultad;
    private $gerenteDAO;
    private $conexion;
    
    
    public function getIdFacultad(){
        return $this->idFacultad;
    }

    public function setIdFacultad($idFacultad){
        $this->idFacultad = $idFacultad;
    }

    public function getGerenteDAO(){
        return $this ->gerenteDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }
    
    function Gerente($id="", $nombre="", $apellido="", $correo="", $password="", $numeroID="", $idtipoIdentificacion="", $idFacultad=""){
            $this -> Persona($id , $nombre, $apellido, $correo, $password, $numeroID, $idtipoIdentificacion);
            $this -> idFacultad = $idFacultad;
            $this -> conexion = new Conexion();
            $this -> gerenteDAO = new GerenteDAO($id, $nombre, $apellido, $correo, $password, $numeroID, $idtipoIdentificacion, $idFacultad);
    }

    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> gerenteDAO -> registrar());
        $this -> conexion -> cerrar();
    }

    function autenticar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> gerenteDAO -> autenticar());
        if($this -> conexion -> numFilas() == 1){
            $registro = $this -> conexion -> extraer(); 
            $this -> id = $registro[0];
            $this -> estado = $registro[1];
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }

    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> gerenteDAO ->actualizar());
        $this -> conexion -> cerrar();
    }
    
    function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> gerenteDAO -> existeCorreo());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }

    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> gerenteDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> numeroID = $resultado[3];
        $this -> idtipoIdentificacion = $resultado[4];
        $this -> idFacultad = $resultado[5];
        $this -> conexion -> cerrar();
    }

    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> gerenteDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Gerente($registro[0], $registro[1], $registro[2], $registro[3], "", $registro[4], $registro[5], $registro[6]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;  
    }
    
}
?>
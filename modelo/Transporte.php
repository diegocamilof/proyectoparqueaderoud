<?php
require 'persistencia/TransporteDAO.php';
require_once 'persistencia/Conexion.php';

class Transporte {
    private $idtransporte;
    private $serial;
    private $modelo;
    private $fotoTransporte;
    private $fotoCartaPropiedad;
    private $descripcion;
    private $estado;
    private $idcolor;
    private $idtipo;
    private $idUsuario;
    private $idmarca;
    private $idParqueadero;
    private $transporteDAO;
    private $conexion;	
   
    public function getIdtransporte(){
        return $this->idtransporte;
    }

    public function getSerial(){
        return $this->serial;
    }

    public function getModelo(){
        return $this->modelo;
    }

    public function getFotoTransporte(){
        return $this->fotoTransporte;
    }

    public function getFotoCartaPropiedad(){
        return $this->fotoCartaPropiedad;
    }

    public function getDescripcion(){
        return $this->descripcion;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function getIdcolor(){
        return $this->idcolor;
    }

    public function getIdtipo(){
        return $this->idtipo;
    }

    public function getIdusuario(){
        return $this->idUsuario;
    }

    public function getMarca(){
        return $this->idmarca;
    }

    public function getIdparqueadero(){
        return $this->idParqueadero;
    }

    public function getTransporteDAO(){
        return $this->transporteDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }

    function Transporte($idtransporte= "" , $serial= "", $modelo= "", $fotoTransporte= "", $fotoCartaPropiedad= "", $descripcion= "",$estado= "", 
        $idcolor= "", $idtipo= "", $idUsuario= "",$idmarca= "", $idParqueadero= ""){
        $this -> idtransporte = $idtransporte;
        $this -> serial = $serial;
        $this -> modelo = $modelo;
        $this -> fotoTransporte = $fotoTransporte;
        $this -> fotoCartaPropiedad = $fotoCartaPropiedad;
        $this -> descripcion = $descripcion;
        $this -> estado = $estado;
        $this -> idcolor = $idcolor;
        $this -> idtipo = $idtipo;
        $this -> idUsuario = $idUsuario;
        $this -> idmarca = $idmarca;
        $this -> idParqueadero = $idParqueadero;
        $this -> conexion = new Conexion();
        $this -> transporteDAO = new TransporteDAO($idtransporte , $serial, $modelo, $fotoTransporte, $fotoCartaPropiedad, $descripcion, $estado, $idcolor, 
                                                    $idtipo, $idUsuario, $idmarca, $idParqueadero);

    }

    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO ->actualizar());
        $this -> conexion -> cerrar();
    }

    function actualizarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO ->actualizarFoto());
        $this -> conexion -> cerrar();
    }

    function actualizarFotoPropiedad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO ->actualizarFotoPropiedad());
        $this -> conexion -> cerrar();
    }

    function existeSerial(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> existeSerial());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }

    function actualizarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO ->actualizarEstado());
        $this -> conexion -> cerrar();
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> serial = $resultado[0];
        $this -> modelo = $resultado[1];
        $this -> fotoTransporte = $resultado[2];
        $this -> fotoCartaPropiedad = $resultado[3];
        $this -> descripcion = $resultado[4];
        $this -> estado = $resultado[5];
        $this -> idcolor = $resultado[6];
        $this -> idtipo = $resultado[7];
        $this -> idmarca = $resultado[8];
        $this -> conexion -> cerrar();
    }

    function consultarTransportesPorId($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTransportesPorId($id));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Transporte($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6], $registro[7], $registro[8], $registro[9], $registro[10],$registro[11]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Transporte($registro[0], $registro[1], $registro[2], $registro[3], "", $registro[4], "", "", $registro[5], $registro[6], "", "");
            
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    function consultarFotosVehiculo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarFotosVehiculo());
        while(($registro = $this -> conexion -> extraer()) != null){
            $this -> fotoTransporte = $registro[0];
            $this -> fotoCartaPropiedad = $registro[1];
        }
        $this -> conexion -> cerrar();
    }
        
}
    

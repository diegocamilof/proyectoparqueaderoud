<?php
require 'persistencia/TipoDAO.php';
require_once 'persistencia/Conexion.php';

class Tipo {
    private $id;
    private $tipo;
    private $tipoDAO;
    private $conexion;
    
    public function getId(){
        return $this->id;
    }

    public function getTipo(){
        return $this->tipo;
    }

    public function getTipoDAO(){
        return $this->tipoDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }

    function Tipo($id="" , $tipo=""){
        $this -> id = $id;
        $this -> tipo = $tipo;
        $this -> conexion = new Conexion();
        $this -> tipoDAO = new TipoDAO($id, $tipo);        
    
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tipoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> tipo = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tipoDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Tipo($registro[0], $registro[1]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
}
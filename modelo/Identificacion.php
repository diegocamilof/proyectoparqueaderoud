<?php
require 'persistencia/IdentificacionDAO.php';
require_once 'persistencia/Conexion.php';

class Identificacion {
    private $id;
    private $nombreTipo;
    private $identificacionDAO;
    private $conexion;
    
    public function getId(){
        return $this->id;
    }
    public function setId($id){
         $this -> id = $id;
         $this -> identificacionDAO = new IdentificacionDAO($id);  
    }

    public function getNombreTipo(){
        return $this->nombreTipo;
    }

    public function getIdentificacionDAO(){
        return $this->identificacionDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }

    function Identificacion($id="" , $nombreTipo=""){
        $this -> id = $id;
        $this -> nombreTipo = $nombreTipo;
        $this -> conexion = new Conexion();
        $this -> identificacionDAO = new IdentificacionDAO($id, $nombreTipo);        
    
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> identificacionDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombreTipo = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> identificacionDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Identificacion($registro[0], $registro[1]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
}
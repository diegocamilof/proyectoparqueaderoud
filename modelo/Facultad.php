<?php
require 'persistencia/FacultadDAO.php';
require_once 'persistencia/Conexion.php';

class Facultad {
    private $id;
    private $nombre;
    private $facultadDAO;
    private $conexion;
    
    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getFacultadDAO()
    {
        return $this->facultadDAO;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    function Tipo($id="" , $nombre=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> conexion = new Conexion();
        $this -> facultadDAO = new FacultadDAO($id, $nombre);        
    
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facultadDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facultadDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Facultad($registro[0], $registro[1]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    
}
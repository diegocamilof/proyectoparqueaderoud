<?php
require 'persistencia/ProyectoDAO.php';
require_once 'persistencia/Conexion.php';

class Proyecto {
    private $id;
    private $nombre;
    private $idFacultad;
    private $proyectoDAO;
    private $conexion;
    
    public function getId()
    {
        return $this->id;
    }
    public function setId($id){
        $this -> id = $id;
        $this -> proyectoDAO = new ProyectoDAO($id); 
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getIdFacultad()
    {
        return $this->idFacultad;
    }

    public function getProyectoDAO()
    {
        return $this->proyectoDAO;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    function Proyecto($id="" , $nombre="", $idFacultad=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> idFacultad = $idFacultad;
        $this -> conexion = new Conexion();
        $this -> proyectoDAO = new ProyectoDAO($id, $nombre, $idFacultad);        
    
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Proyecto($registro[0], $registro[1], $registro[2]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    
}
<?php
$error = 0;
$correo = $_POST["correo"];
$password = $_POST["password"];
$usuario = new Usuario("", "", "", $correo, $password);
$celador = new Celador("", "", "", $correo, $password);
$gerente = new Gerente("", "", "", $correo, $password);
$administrador = new Administrador("", "", "", $correo, $password);
if($administrador -> autenticar()){
    $_SESSION['id'] = $administrador -> getId();
    $_SESSION["rol"] = "administrador";
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
}else if($usuario -> autenticar()){
    if($usuario -> getEstado() == 1 || $usuario -> getEstado() == 2){
        $_SESSION["id"] = $usuario -> getId();
        $_SESSION["rol"] = "usuario";
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionUsuario.php"));
    }else{
        header("Location: index.php?error=2");
    }
}else if($celador -> autenticar()){
	$_SESSION["id"] = $celador -> getId();
	$_SESSION["rol"] = "celador";
	header("Location: index.php?pid=" . base64_encode("presentacion/sesionCelador.php"));
}else if($gerente -> autenticar()){
	$_SESSION["id"] = $gerente -> getId();
	$_SESSION["rol"] = "gerente";
	header("Location: index.php?pid=" . base64_encode("presentacion/sesionGerente.php"));
}else{
    header("Location: index.php?error=1");
}

?>

<!--
require 'logica/Persona.php';
require 'logica/Administrador.php';
	$correo = "";
	if (isset($_POST['correo'])) {
		$correo = $_POST['correo'];
	}
	$clave = "";
	if (isset($_POST['clave'])) {
		$clave = $_POST['clave'];
	}

	if (isset($_POST['autenticar'])) {

		$administrador = new Administrador("", "", "", $correo, $clave);
		if ($administrador -> autenticar()) {
			$_SESSION['id'] = $administrador -> getId();
			$_SESSION['rol'] = "administrador";
			$pid = base64_encode("presentacion/sesionAdministrador.php");
			header('Location: index.php?pid=' . $pid);
		} else {
	
				$usuario = new Usuario("", "", "", $correo, $clave);
					if ($usuario -> autenticar()) {
						$_SESSION['id'] = $usuario -> getId();
						$_SESSION['rol'] = "usuario";
						$pid = base64_encode("presentacion/sesionUsuario.php");
						header('Location: index.php?pid=' . $pid);
					}
		}
	}
?>
-->
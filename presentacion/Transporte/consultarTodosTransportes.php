<?php 
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
    include "presentacion/menuAdministrador.php";
}
$transporte = new Transporte();
$transportes = $transporte->consultarTodos();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Consultar Usuario</title>
</head>
<body>
	<div class="consultaCard">
		<h1 class="tituloConsulta mb-2">CONSULTAR TRANSPORTE</h1>
		<div class="consultarr">
			<table class="table">
				<thead class="table-light"> 
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Serial</th>
						<th scope="col">Modelo</th>
						<th scope="col">Foto</th>

						<th scope="col">Estado</th>
						<th scope="col">Marca</th>
						<th scope="col">Servicios</th>
					</tr>
				</thead>
				<tbody>
					<?php
                		foreach ($transportes as $t) {
							echo "<tr>";
							echo "<td>" . $t->getId() . "</td>";
							echo "<td>" . $t->getSerial() . "</td>";
							echo "<td>" . $t->getModelo() . "</td>";
							echo "<td>" . (($t->getFoto()!="")?"<img src='/IPSUD/fotos/" . $t->getFoto() . "' height='50px'>":"") . "</td>";

							echo "<td><div id=estado" .$t->getId() . "><span  class='fas " . ($t->getEstado()==0?"fa-times-circle":"fa-check-circle") . "' data-toggle='tooltip' class='tooltipLink' data-placement='left' data-original-title='" . ($t->getEstado()==0?"Inhabilitado":"Habilitado") . "' ></span>" . "</div></td>";
							echo "<td>" . $t->getIdMarca() . "</td>";
							
							echo "<td>" . "<a href='modalUsuario.php?idUsuario=" . $t->getId() . "' data-toggle='modal' data-target='#modalPaciente' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' data-original-title='Ver Detalles' ></span> </a>
										<a class='fas fa-pencil-ruler' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarUsuario-admin.php") . "&idusuario=" . $t->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar'> </a>
										<a class='fas fa-camera' href='index.php?pid=" . base64_encode("presentacion/usuario/actualizarFotoUsuario.php") . "&idusuario=" . $t->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar Foto'> </a>
										<a id='cambiarEstado" . $t->getId() . "' class='fas fa-power-off' href='#' data-toggle='tooltip' data-placement='left' title='" . ($t->getEstado()==0?"Habilitar":"Inhabilitar") . "'> </a>
								</td>";
							echo "</tr>";
                		}
                	echo "<caption>" . count($transportes) . " registros encontrados</caption>"?>
				</tbody>
			</table>
		</div>
	</div>

<!--SCRIPTS ADICIONALES-->
<div class="modal fade" id="modalPaciente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	var modalUsuario = document.getElementById('modalUsuario')
	exampleModal.addEventListener('show.bs.modal', function (event) {
  var button = event.relatedTarget
  var recipient = button.getAttribute('data-bs-whatever')
  var modalTitle = exampleModal.querySelector('.modal-title')
  var modalBodyInput = exampleModal.querySelector('.modal-body input')

  modalTitle.textContent = 'New message to ' + recipient
  modalBodyInput.value = recipient
})
</script>


<script type="text/javascript">
$(document).ready(function(){
	<?php foreach ($usuarios as $u) { ?>
	$("#cambiarEstado<?php echo $u -> getId(); ?>").click(function(e){
		e.preventDefault();
		<?php echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/usuario/editarEstadoUsuarioAjax.php") . "&idusuario=" . $p -> getId() . "&estado=" . (($p -> getEstado() == 0)?"1":"0") . "\";\n"; ?>
		$("#estado<?php echo $p -> getId(); ?>").load(ruta);
	});
	<?php } ?>
});
</script>


	


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
<?php
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();
include 'presentacion/menuAdministrador.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">
    <title>Document</title>
</head>
<body>
<br>
    <h1>Bienvenido, señor Administrador: <?php echo $administrador -> getNombre() . " " . $administrador -> getApellido() ?> </h1>
    
</body>
</html>
<?php 

if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
    include "presentacion/menuAdministrador.php";
}

$usuario = new Usuario();
$usuarios = $usuario->consultarTodos();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Consultar Usuario</title>
</head>
<body>
	<div class="consultaCard">
		<h1 class="tituloConsulta mb-2">CONSULTAR ESTUDIANTES</h1>
		<div class="consultarr">
    
        <div class="card" style="width: 18rem;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                    <?php
					    $p = new Proyecto();
						$pos = 1;
                		foreach ($usuarios as $u) {
							echo "<h6>" . $pos ++ . "</h6>";
							echo "<h2 class='card-title'>" . $u->getNombre() . " " . $u->getApellido() . "</h2>";
							echo "<p class='card-text'>" . $u->getCorreo() . "</p>";
							echo "<p class='card-text'>" . $u->getCodigoEstudiantil() . "</p>"; 
                            echo "<br>";
							echo "<p><span id='estado" .$u->getId() ."'  class='fas " . ($u->getEstado()==0?"fa-times-circle":(($u->getEstado()==1)?"fa-check-circle":"fa-exclamation-triangle")) . "' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='" . ($u->getEstado()==0?"Inhabilitado":($u->getEstado()==1)?"Habilitado":"Pendiente") . "' ></span>" . "</p>";
							$p -> setId($u -> getIdproyecto());
							$p -> consultar();
							echo "<p class='card-text'>" . $p -> getNombre() . "</p>";
                            echo "<tr>";
							echo "<td>" . "<a href='modalUsuario.php?idUsuario=" . $u->getId() . "' data-bs-toggle='modal' data-bs-target='#modalPaciente' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a>
										<a class='fas fa-pencil-ruler' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarUsuario-admin.php") . "&idusuario=" . $u->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar'>  </a>"
										.($u -> getEstado()<=1?" <a id='cambiarEstado" . $u->getId() . "' class='fas " .($u->getEstado()==0? "fa-user-check": "fa-user-times"). "' href='#' data-toggle='tooltip' data-placement='left'> </a>":"") .
								"</td>"; 
							echo "</tr>";
                		}
                	echo "<caption>" . count($usuarios) . " registros encontrados</caption>"?>

                <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
		</div>
	</div>

<!--SCRIPTS ADICIONALES-->
<div class="modal fade" id="modalPaciente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>


<script type="text/javascript">
	<?php 
		foreach ($usuarios as $u) {
			if($u -> getEstado() !=2){
				echo "$('#cambiarEstado" . $u -> getId() . "').click(function() {\n";
				echo "\tvar url = 'indexAjax.php?pid=" . base64_encode("presentacion/usuario/cambiarEstadoUsuarioAjax.php") . "&id=" . $u -> getId() . "';\n";
				echo "\t$('#cambiarEstado" . $u -> getId() . "').load(url);\n";
				echo "\tif($('#cambiarEstado" . $u -> getId() . "').attr('class') == 'fas fa-user-times'){\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'fas fa-user-check');\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('title', 'Habilitar');\n";
				echo "\t}else{\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'fas fa-user-times');\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('title', 'Desabilitar');\n";
				echo "\t}\n";
				echo "\tif($('#estado" . $u -> getId() . "').attr('class') == 'fas fa-times-circle'){\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-check-circle');\n";
				echo "\t}else{\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-times-circle');\n";
				echo "\t}\n";
				echo "});\n";

				
				        
			}
		}
	?>
</script>


	


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
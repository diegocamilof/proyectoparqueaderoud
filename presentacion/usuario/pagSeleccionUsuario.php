<?php 
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();
include "presentacion/menuAdministrador.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Usuario</title>
</head>
<body>

<div class="containerCards mt-5">

    <div class="card containerCards-elegir">
        <div class="row g-0">
            <div class="col-md-5 containerCards-elegir-imagen">
                <i class="fas fa-user"></i>
            </div>
            <div class="col-md-7">
                <div class="card-body containerCards-elegir-info">
                    <h5 class="card-title containerCards-elegir-info-titulo">Crear Estudiantes</h5>  
                    <div class="card-title containerCards-elegir-info-botonCard">
                        <a class="containerCards-elegir-info-botonCard-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/crearUsuario.php")?>">Crear Usuarios</a>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div class="card containerCards-elegir mt-5">
        <div class="row g-0">
            <div class="col-md-5 containerCards-elegir-imagen">
                <i class="fas fa-search"></i>
            </div>
            <div class="col-md-7">
                <div class="card-body containerCards-elegir-info">
                    <h5 class="card-title containerCards-elegir-info-titulo">Consultar Estudiantes</h5>
                    <div class="card-title containerCards-elegir-info-botonCard">
                        <a class="containerCards-elegir-info-botonCard-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuario.php")?>">Consultar Usuarios</a>
                    </div>    
                </div>
            </div>
        </div>
    </div>

    

</div>




	


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
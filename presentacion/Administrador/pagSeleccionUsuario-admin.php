<?php 
    $administrador = new Administrador($_SESSION['id']);
    $administrador->consultar();
    include 'presentacion/menuAdministrador.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Usuario</title>
</head>
<body>

<div class="containerCards mt-5">
<h1>APARTADO USUARIOS</h1>
    <div class="card card-x mt-5">
        <div class="row g-0">
            <div class="col-md-4 imagen">
                <img src="./img/profile.png" alt="">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">Crear Usuarios</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    <a class="linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Administrador/crearUsuario-admin.php")?>">Crear Usuarios</a>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-x mt-5">
        <div class="row g-0">
            <div class="col-md-4 imagen">
                <img class="ml-5"src="./img/buscar.png" alt="">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">Consultar Usuario</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    <a class="linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Administrador/consultarUsuario-admin.php")?>">Consultar Usuarios</a>
                </div>
            </div>
        </div>
    </div>

    

</div>




	


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
<?php 
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();

$usuario = new Usuario($_GET["idusuario"]);
$usuario -> consultar();
if (isset($_POST["actualizar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $codigoEstudiantil = $_POST["codigoEstudiantil"];
    $direccion = $_POST["direccion"];
    $telefono = $_POST["telefono"];
    $numeroID = $_POST["numeroID"];
    $idProyecto = $_POST["idProyecto"];
    $idTipoIdentificacion = $_POST["idTipoIdentificacion"];
    $usuario = new Usuario($_GET["idusuario"], $nombre, $apellido, "", "", $codigoEstudiantil, $direccion, $telefono, "", "", $numeroID, $idProyecto, $idTipoIdentificacion);
    $usuario->actualizar();
}
include 'presentacion/menuAdministrador.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Actualizar Usuario</title>
</head>
<body>
    <h1 class="titulosUsuario">ACTUALIZAR USUARIO</h1>

    <div class="crearTraan">
        <?php if (isset($_POST["actualizar"])) { ?>
            <div class="alert alert-success" role="alert">Usuario actualizado exitosamente.</div>						
        <?php } ?>
    <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/Administrador/actualizarUsuario-admin.php")."&idusuario=".$_GET["idusuario"] ?> method="post">
        <div class="row">
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="exampleFormControlInput1">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="exampleFormControlInput1" 
                placeholder="Escribe tu nombre" value="<?php echo $usuario->getNombre(); ?>">
            </div>
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="exampleFormControlInput1">Apellido</label>
                <input type="text" name="apellido" class="form-control" id="exampleFormControlInput1" 
                placeholder="Escribe tu Apellido" value="<?php echo $usuario->getApellido(); ?>">
            </div> 
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="exampleFormControlInput1">Código estudiantial</label>
                <input type="number" name="codigoEstudiantil" class="form-control" id="exampleFormControlInput1" 
                placeholder="Escribe tu Código estudiantial" value="<?php echo $usuario->getCodigoEstudiantil(); ?>">
            </div>
        </div>
        <div class="row mt-2">
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="exampleFormControlInput1">Direccion</label>
                <input type="text" name="direccion" class="form-control" id="exampleFormControlInput1" 
                                placeholder="Escribe tu direccion" value="<?php echo $usuario->getDireccion(); ?>">
            </div>
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="exampleFormControlInput1">Telefono</label>
                <input type="number" name="telefono" class="form-control" id="exampleFormControlInput1" 
                                placeholder="Escribe tu teléfono" value="<?php echo $usuario->getTelefono(); ?>">
            </div>
        </div>
        <div class="row mt-2">
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="inputState">Proyecto curricular del estudiante?</label>
                    <select id="inputState" class="form-control" name="idProyecto">
                        <?php
                            $proyecto = new Proyecto();
                            $proyectos = $proyecto -> consultarTodos();
                            foreach ($proyectos as $p) {
                                echo "<option value='" . $p->getId() . "'>" . $p->getNombre() . "</option>";
                            }
                        ?>
                    </select>
            </div>
            <div class="form-group col-md-4">
                <label id="crearTrasnporte-label" for="exampleFormControlInput1">Tipo de identificación</label>
                    <select id="inputState" class="form-control" name="idTipoIdentificacion">
                        <?php
                            $identificacion = new Identificacion();
                            $identificacions = $identificacion -> consultarTodos();
                            foreach ($identificacions as $i) {
                                echo "<option value='" . $i->getId() . "'>" . $i->getNombreTipo() . "</option>";
                            }
                        ?>
                    </select>
            </div>
            <div class="form-group col-md-3">
                <label id="crearTrasnporte-label" for="exampleFormControlInput1">Número de identificación</label>
                <input type="number" name="numeroID" class="form-control" id="exampleFormControlInput1" placeholder="Escribe tu número de identificación" value="<?php echo $usuario->getTelefono(); ?>">
            </div>
        </div>
        <div class="container mt-4 mb-1 d-flex justify-content-between">
            <div class="b">
                <a class="b-botonVolver" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuario.php")?>">Volver</a>
            </div>
            <div class="">
                <button class="botonCrearTransporte" name="actualizar">Actualizar</button>
            </div>
        </div>

    </form>
    
</div>
        
            


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>       
</body>
</html>
<?php
$usuario = new Usuario($_SESSION['id']);
$usuario->consultar();
include 'presentacion/usuario/menuUsuario.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssUsuario/stylesUsuario.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Sesión Usuario</title>
</head>
<body>  
    <h1 class="titulosUsuario">Bienvenido, estudiante: <?php echo $usuario -> getNombre() . " " . $usuario -> getApellido() ?> </h1>
        <div class="cardInfoUsuario mt-3">
            <div class="card card-x">
                <div class="row g-0">
                    <div class="col-md-6 mt-1 imagen">
                        <img src="./img/profile.png" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="card-body">
                            <p class="card-text">
                                <?php echo $usuario ->  getCodigoEstudiantil() ?>
                                <?php echo $usuario -> getCorreo()?>
                                <?php echo $usuario -> getIdproyecto() ?>
                            </p>
                            <a class="linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/actualizarTransporte.php")?>">Actualizar Transporte</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>

<?php
require_once 'modelo/Persona.php';
require_once 'modelo/Usuario.php';

$idusuario = $_GET['idUsuario'];
$usuario = new Usuario($idusuario);
$usuario->consultar();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssModal/styleModal.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Consultar Usuario</title>
</head>
<body>
	<div class="backgroundModal">
					<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Desatalles Estudiante</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<div class="tabla">
							<table class="table">
								<tbody>
									<tr>
										<th width="20%">Nombre</th>
										<td><?php echo $usuario -> getNombre(); ?></td>
									</tr>		
									<tr>
										<th width="20%">Apellido</th>
										<td><?php echo $usuario -> getApellido(); ?></td>
									</tr>
									<tr>
										<th width="20%">Foto</th>
										<td><?php echo $usuario -> getFoto()==""?"<img src=/proyectoparqueadero/img/profile.png>":"<img src=/IPSUD/fotos/".$usuario -> getFoto(). "/>"; ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
